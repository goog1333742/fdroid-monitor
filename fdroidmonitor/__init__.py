# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

VERSION = 'v0.6.2'
